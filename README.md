﻿齐博X1.0
===========================

齐博X1.0是集众家之所长再结合自身特色而开发出来的一套开源PHP系统。


在此非常感谢ThinkPHP提供那么优秀的免费开源底层，同时也要感谢DolphinPHP提供那么好用的ZBuilder编辑器，除此以外还要特别鸣谢layer、mui、bootstrap、jquery、ueditor等等开源底层JS脚本。

同时也要感谢一下YZNCMS、YFCMF、hisiphp、fastadmin、ThinkCMF、cltphp、EacooPHP、
lyadmin、sentcms、spfcms等等为PHP开源所做的贡献.

最后还要感谢一下 中电云集 http://www.chinaccnet.com/ 一直以来提供免费服务器赞助支持!

===========================

齐博X1.0是参阅众家之所长而开发出来的系统，秉承PHP开源精神，齐博X1.0框架永久开源免费使用（也可免费用于商业），不区分商业版与免费版。

但为了能持续发展，引入更多的开发者，后续的插件与模块会有免费与收费的区别。
