<?php

// 资源路径配置
return [
    'core_js' => [ // 默认加载
        "/public/static/js/core/jquery.min.js",
        "/public/static/js/core/bootstrap.min.js",
        "/public/static/js/core/jquery.slimscroll.min.js",
        "/public/static/js/core/jquery.scrollLock.min.js",
        "/public/static/js/core/jquery.appear.min.js",
        "/public/static/js/core/jquery.countTo.min.js",
        "/public/static/js/core/jquery.placeholder.min.js",
        "/public/static/js/core/js.cookie.min.js",
        "/public/static/libs/magnific-popup/magnific-popup.min.js",
        "/public/static/js/app.js",
        "/public/static/js/dolphin.js",
        "/public/static/js/builder/form.js",
        "/public/static/js/builder/aside.js",
        "/public/static/js/builder/table.js",
    ],
    'core_css' => [ // 默认加载
        "/public/static/libs/magnific-popup/magnific-popup.min.css",
        "/public/static/admin/css/admin/css/bootstrap.min.css",
        "/public/static/admin/css/admin/css/oneui.css",
        "/public/static/admin/css/admin/css/dolphin.css",
    ],
    'libs_js' => [ // 默认加载
        "/public/static/libs/bootstrap-notify/bootstrap-notify.min.js",
        "/public/static/libs/sweetalert/sweetalert.min.js",
    ],
    'libs_css' => [ // 默认加载
        "/public/static/libs/sweetalert/sweetalert.min.css",
    ],
    'datepicker_js' => [ // 日期选择
        "/public/static/libs/bootstrap-datepicker/bootstrap-datepicker.min.js",
        "/public/static/libs/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js",
    ],
    'datepicker_css' => [ // 日期选择
        "/public/static/libs/bootstrap-datepicker/bootstrap-datepicker3.min.css",
    ],
    'datetimepicker_js' => [ // 日期时间选择
        "/public/static/libs/bootstrap-datetimepicker/moment.min.js",
        "/public/static/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js",
        "/public/static/libs/bootstrap-datetimepicker/locale/zh-cn.js",
    ],
    'moment_js' => [
        "/public/static/libs/bootstrap-datetimepicker/moment.min.js",
    ],
    'datetimepicker_css' => [ // 日期时间选择
        "/public/static/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"
    ],
    'webuploader_js' => [ // 文件或图片上传
        "/public/static/libs/webuploader/webuploader.min.js",
    ],
    'webuploader_css' => [ // 文件或图片上传
        "/public/static/libs/webuploader/webuploader.css",
    ],
    'select2_js' => [ // 下拉框
        "/public/static/libs/select2/select2.full.min.js",
        "/public/static/libs/select2/i18n/zh-CN.js",
    ],
    'select2_css' => [ // 下拉框
        "/public/static/libs/select2/select2.min.css",
        "/public/static/libs/select2/select2-bootstrap.min.css",
    ],
    'tags_js' => [ // 标签
        "/public/static/libs/jquery-tags-input/jquery.tagsinput.min.js",
    ],
    'tags_css' => [ // 标签
        "/public/static/libs/jquery-tags-input/jquery.tagsinput.min.css",
    ],
    'validate_js' => [ // 验证
        "/public/static/libs/jquery-validation/jquery.validate.min.js",
    ],
    'editable_js' => [ // 快速编辑
        "/public/static/libs/bootstrap3-editable/js/bootstrap-editable.js",
    ],
    'editable_css' => [ // 快速编辑
        "/public/static/libs/bootstrap3-editable/css/bootstrap-editable.css",
    ],
    'colorpicker_js' => [ // 取色器
        "/public/static/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js",
    ],
    'colorpicker_css' => [ // 取色器
        "/public/static/libs/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css",
    ],
    'editormd_js' => [ // markdown编辑器
        "/public/static/libs/editormd/editormd.min.js",
    ],
    'jcrop_js' => [ // 图片裁剪
        "/public/static/libs/jcrop/js/Jcrop.min.js",
    ],
    'jcrop_css' => [ // 图片裁剪
        "/public/static/libs/jcrop/css/Jcrop.min.css",
    ],
    'masked_inputs_js' => [ // 格式文本
        "/public/static/libs/masked-inputs/jquery.maskedinput.min.js",
    ],
    'rangeslider_js' => [ // 范围
        "/public/static/libs/ion-rangeslider/js/ion.rangeSlider.min.js",
    ],
    'rangeslider_css' => [ // 范围
        "/public/static/libs/ion-rangeslider/css/ion.rangeSlider.min.css",
        "/public/static/libs/ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css",
    ],
    'nestable_js' => [ // 拖拽排序
        "/public/static/libs/jquery-nestable/jquery.nestable.js",
    ],
    'nestable_css' => [ // 拖拽排序
        "/public/static/libs/jquery-nestable/jquery.nestable.css",
    ],
    'wangeditor_js' => [ // wang编辑器
        "/public/static/libs/wang-editor/js/wangEditor.min.js",
    ],
    'wangeditor_css' => [ // wang编辑器
        "/public/static/libs/wang-editor/css/wangEditor.min.css",
    ],
    'summernote_js' => [ // summernote编辑器
        "/public/static/libs/summernote/summernote.min.js",
        "/public/static/libs/summernote/lang/summernote-zh-CN.js",
    ],
    'summernote_css' => [ // summernote编辑器
        "/public/static/libs/summernote/summernote.min.css",
    ],
    'jqueryui_js' => [ // jqueryui
        "/public/static/libs/jquery-ui/jquery-ui.min.js",
    ]
];